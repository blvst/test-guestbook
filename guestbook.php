<?php
    require_once './mysql/MysqliDb.php';

    class GuestBook {

        public $errors = [];
        public $ppp = 10;
        private $db;

        function __construct() {
            $this->db = new MysqliDb ('localhost', 'login', 'password', 'bd_name');
        }

        function getList($current) {
            $this->db->orderBy('id', 'Desc');

            return $this->db->get ('messages', [$current, $this->ppp]);
        }

        function setPost($author, $message) {
            $post = [
                'author' => $this::clear($author),
                'message' => $this::clear($message)
            ];

            $id = false;
            if ($this->validatePost($post)) {
                $id = $this->db->insert ('messages', $post);
            }

            if (!$id) {
                $this->errors['db'] = 'Сообщение не добавлено в базу данных!';
                return false;
            }

            return true;
        }

        function validatePost($post) {
            $done = true;

            if ($post['author'] == '') {
                $this->errors['author'] = 'Не заполнено имя';
                $done = false;
            }

            if ($post['message'] == '') {
                $this->errors['message'] = 'Не заполнено сообщение';
                $done = false;
            }

            return $done;
        }

        function countPages() {
            return ceil($this->db->getValue ('messages', 'count(*)') / $this->ppp ) ;
        }

        private static function clear($val) {
            $val = trim($val);
            $val = strip_tags($val);
            $val = htmlspecialchars($val);

            return $val;
        }

    }