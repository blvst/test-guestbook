<?php
    require_once 'guestbook.php';
    $gb = new GuestBook();

    if ($_SERVER["REQUEST_METHOD"] == 'POST') {
        $author = $_POST['author'] ?? '';
        $message = $_POST['message'] ?? '';

        if ($gb->setPost($author, $message)) {
            header('Location: list.php');
        }
    }

?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>GuestBook</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="wrapper">
        <?php foreach ($gb->errors as $error) { ?>
            <div class="error"><?php echo $error; ?></div>
        <?php } ?>
        <form action="/index.php" method="post">
            <input type="text" placeholder="Введите имя" name="author">
            <textarea placeholder="Введите сообщение" name="message"></textarea>
            <button type="submit">Отправить</button>
        </form>
        <div class="nav">
            <a href="./list.php">Посмотреть все сообщения</a>
        </div>
    </div>

</body>
</html>

