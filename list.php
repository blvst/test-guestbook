<?php
    require_once 'guestbook.php';

    $start = $_GET['start'] ?? 0;
    $gb = new GuestBook();
    $list = $gb->getList($start);

?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>GuestBook</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="wrapper">
        <div class="nav">
            <a href="./">Оставить сообщение</a>
        </div>
        <div class="list">
            <?php foreach($list as $post) { ?>
                <div class="post">
                    <div class="author">
                        <b><?php echo $post['author'] ?></b>
                    </div>
                    <div class="message"><?php echo $post['message'] ?></div>
                </div>
            <?php } ?>
        </div>
        <div class="nav">
            <?php for ($i = 0; $i < $gb->countPages(); $i++) { ?>
                <?php if ($i == $start) { ?>
                    <span><?php echo $i+1 ?></span>
                <?php } else { ?>
                    <a href="?start=<?php echo $i * $gb->ppp ?>"><?php echo $i+1 ?></a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</body>
</html>
